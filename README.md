![C/C++ CI](https://github.com/chihming/smore-stack/workflows/C/C++%20CI/badge.svg?branch=master)

# smore-stack
Now Refactoring ...

### Compilation
```
make
```

### Structure
```
.
├── hub
│   └── mf.cpp
├── LICENSE
├── Makefile
├── README.md
└── src
    ├── mapper
    │   ├── lookup_mapper.cpp
    │   └── lookup_mapper.h
    ├── optimizer
    │   ├── pair_optimizer.cpp
    │   └── pair_optimizer.h
    ├── sampler
    │   ├── alias_methods.cpp
    │   ├── alias_methods.h
    │   ├── vc_sampler.cpp
    │   └── vc_sampler.h
    └── util
        ├── file_graph.cpp
        ├── file_graph.h
        ├── hash.cpp
        ├── hash.h
        ├── random.cpp
        ├── random.h
        ├── util.cpp
        └── util.h
```

### Contribution
See from the issues
